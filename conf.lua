-- Configuration
function love.conf(t)
	t.title = 'Tic-Tac-Toe'
	t.version = '0.10.1'
	t.window.width = 400
	t.window.height = 440
	t.window.icon = 'icon.png'
end
