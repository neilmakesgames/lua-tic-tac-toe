require 'grid'
require 'gamemanager'
require 'colors'

function love.load(arg)
  grid:init(20, 60, 360, 360)
  gamemanager:init()
  love.graphics.setBackgroundColor(colors.bg)
end

function love.update(dt)

end

function love.draw(dt)
  grid:draw(dt)
  gamemanager:draw(dt)
end

function love.mousepressed(x, y, button, istouch)
  gamemanager:handleClick(x, y)
end

function love.keypressed(key)
  if key == 'r' then
    gamemanager:reset()
  elseif key == 'escape' then
    love.event.quit()
  end
end
