-- messageManager

messageManager = {}

-- init
function messageManager:init(x, y, color)
  self.messages = {}
  self.x = x or 0
  self.y = y or 0
  self.color = color or colors.gray
  self.font = love.graphics.newFont(20)
  love.graphics.setFont(self.font)
  self.fontHeight = self.font:getHeight()
  self.align = 'center'
  return self
end

-- draw
function messageManager:draw()
  local drawx = self.x
  local drawy = self.y

  for i in pairs(self.messages) do
    local msg = self.messages[i]
    love.graphics.setColor(self.color)
    love.graphics.printf(msg, drawx, drawy, love.graphics.getWidth(), self.align)
    drawy = drawy + self.fontHeight
  end
end

-- message functions
function messageManager:push(message)
  table.insert(self.messages, message)
end

function messageManager:pop()
  table.remove(self.messages)
end

function messageManager:shift()
  table.remove(self.messages, 1)
end

function messageManager:empty()
  for i in pairs(self.messages) do
    self.messages[i] = nil
  end
end
