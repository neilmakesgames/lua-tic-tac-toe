-- gamemanager.lua
require "messageManager"

gamemanager = {}

function gamemanager:init()
  -- messages
  self.mm = messageManager:init(0, 20, colors.gray)

  -- players
  self.players = {}
  self.players.x = {
    symbol = 'X',
    color = colors.red
  }
  self.players.o = {
    symbol = 'O',
    color = colors.blue
  }
  self:reset()
end

function gamemanager:reset()
  self.gameover = false
  self.victory = nil
  self.turnNumber = 0
  self.whoseTurn = self.players.o
  grid:reset()
  self:advanceTurn()
end

function gamemanager:draw(dt)
  self.mm:draw(dt)
end

-- advance turn -------------------------------------------
function gamemanager:advanceTurn()
  self.victory = grid:checkVictory()
  self.turnNumber = self.turnNumber + 1

  if self.turnNumber > 9 or self.victory ~=nil then
    self.gameover = true
    self:doGameOver()
  else
    self:doTurn()
  end


end

function gamemanager:doTurn()
  -- switch to other player's turn:
  if self.whoseTurn == self.players.x then
    self.whoseTurn = self.players.o
  else
    self.whoseTurn = self.players.x
  end

  local msg = 'Turn ' .. self.turnNumber .. '. '
  msg = msg .. self.whoseTurn.symbol .. " player's turn."

  self.mm:empty()
  self.mm:push(msg)
end

function gamemanager:doGameOver()
  local msg
  if self.victory == nil then
    msg = 'Draw. Press [R] to restart.'
  else
    msg = self.victory .. " wins. Press [R] to restart."
  end
  self.mm:empty()
  self.mm:push(msg)
end

-- handle click -------------------------------------------
function gamemanager:handleClick(x, y)
  if self.gameover == false and pointInRect(x, y, grid.x, grid.y, grid.width, grid.height) then
    local xy = grid:translatePixelsToGrid(x - grid.x, y - grid.y)
    local tile = grid:getTile(xy.x, xy.y)
    -- self.mm:push(tile) -- debugging
    if tile == nil then
      grid:setTile(xy.x, xy.y, self.whoseTurn.symbol, self.whoseTurn.color)
      self:advanceTurn()
    else

    end
  end
end

-- utils --------------------------------------------------
function pointInRect(x1, y1, x2, y2, w2, h2)
  return x1 >= x2 and x1 <= x2 + w2 and y1 >= y2 and y2 <= y2 + h2
end
