-- grid.lua
grid = {}

function grid:init(x, y, width, height, color, linewidth)
  self.x = x or 20
  self.y = y or 20
  self.width = width or love.graphics.getWidth() - 40
  self.height = height or love.graphics.getHeight() - 40
  self.color = color or colors.gray
  self.tilesize = math.floor(self.width * .33)
  self.tilepadding = 10
  self.glyphlinewidth = 8
  self.lines = {
    {self.tilesize, 0, self.tilesize, self.height},
    {self.tilesize * 2, 0, self.tilesize * 2, self.height},
    {0, self.tilesize, self.width, self.tilesize},
    {0, self.tilesize * 2, self.width, self.tilesize * 2}
  }
  self.linewidth = linewidth or 4;
  -- tile grid
  self.tiles = {
    {nil, nil, nil},
    {nil, nil, nil},
    {nil, nil, nil}
  }
  -- ugly, but who cares:
  self.victorysets = {
    -- vertical
    {{1,1},{1,2},{1,3}},
    {{2,1},{2,2},{2,3}},
    {{3,1},{3,2},{3,3}},
    -- horizontal
    {{1,1},{2,1},{3,1}},
    {{2,1},{2,2},{3,2}},
    {{3,1},{3,2},{3,3}},
    -- diagonal
    {{1,1},{2,2},{3,3}},
    {{3,1},{2,2},{1,3}}
  }
  self.victorlineset = nil

end

function grid:reset()
  self.tiles = {
    {nil, nil, nil},
    {nil, nil, nil},
    {nil, nil, nil}
  }
  self.victorlineset = nil
end

-- drawing functions --------------------------------------
function grid:draw(dt)
  -- draw grid
  love.graphics.setColor(self.color)
  love.graphics.setLineWidth(self.linewidth)
  for i, v in ipairs(self.lines) do
    love.graphics.push()
    love.graphics.translate(self.x, self.y)
    love.graphics.line(v)
    love.graphics.pop()
  end

  self:drawTiles(dt)

  if self.victorlineset ~= nil then
    self:drawVictorLine()
  end
end

function grid:drawTiles(dt)
  local drawx = self.x
  local drawy = self.y
  for y=1,3 do
    for x=1,3 do
      self:drawTile(self.tiles[y][x], drawx, drawy)
      drawx = drawx + self.tilesize
    end
    drawx = self.x
    drawy = drawy + self.tilesize
  end
end

function grid:drawTile(tile, drawx, drawy)
  if tile ~= nil then
    love.graphics.setColor(tile.color)
    love.graphics.setLineWidth(self.glyphlinewidth)

    if tile.symbol == 'X' then
      self:drawTileX(drawx, drawy)
    elseif tile.symbol == 'O' then
      self:drawTileO(drawx, drawy)
    else
      love.graphics.print(tile.symbol, drawx, drawy)
    end
  end
end

function grid:drawTileX(drawx, drawy)
  local ts = self.tilesize
  local p = self.tilepadding
  local x = drawx
  local y = drawy

  -- draw \ stroke
  love.graphics.line(
    x + p, y + p,
    x + ts - p , y + ts - p
  )
  -- draw / stroke
  love.graphics.line(
    x + p, y + ts - p,
    x + ts - p, y + p
  )
end

function grid:drawTileO(drawx, drawy)
  love.graphics.circle(
    'line',
    drawx + self.tilesize/2,
    drawy + self.tilesize/2,
    self.tilesize/2 - self.tilepadding,
    32
  )
end

function grid:drawVictorLine()

  love.graphics.setColor(colors.green)
  love.graphics.setLineWidth(self.glyphlinewidth)
  love.graphics.push()
  love.graphics.translate(self.x, self.y)
  love.graphics.line(self.victorlineset)
  love.graphics.pop()

end

-- tile getters and setters -------------------------------
function grid:getTileByPixel(px, py)
  local x = math.floor(px / self.tilesize)
  local y = math.floor(py / self.tilesize)
  return self.tiles[y+1][x+1]
end

function grid:translatePixelsToGrid(px, py)
  local x = math.floor(px / self.tilesize)
  local y = math.floor(py / self.tilesize)
  return {x = x + 1, y = y + 1}
end

function grid:setTile(x, y, symbol, color)
  self.tiles[y][x] = {}
  self.tiles[y][x].symbol = symbol
  self.tiles[y][x].color = color or {128, 128, 128}
end

function grid:getTile(x, y)
  return self.tiles[y][x]
end

-- check for victory --------------------------------------

function grid:checkVictory()
  local victor = nil
  for i=1,#self.victorysets do
    local set = self.victorysets[i]
    local a = grid.tiles[set[1][1]][set[1][2]]
    local b = grid.tiles[set[2][1]][set[2][2]]
    local c = grid.tiles[set[3][1]][set[3][2]]

    victor = grid:checkSet(a, b, c)
    if victor ~= nil then
      local ts = self.tilesize
      local p = self.tilepadding
      self.victorlineset = {
        (set[1][2] - 1) * ts + (ts * .5),
        (set[1][1] - 1) * ts + (ts * .5),
        (set[3][2] - 1) * ts + (ts * .5),
        (set[3][1] - 1) * ts + (ts * .5)
      }
      break
    end
  end
  return victor
end

function grid:checkSet(a, b, c)
  if a == nil or b == nil or c == nil then
    return nil
  elseif a.symbol == b.symbol and b.symbol == c.symbol then
    return a.symbol
  else
    return nil
  end
end
